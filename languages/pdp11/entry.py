import subprocess
from subprocess import Popen, PIPE
from time import sleep
import sys
from subprocess import PIPE, Popen
from threading  import Thread
from queue import Queue, Empty
import delegator
import os
import select
import pty
import tty

def run_command(command):
    subprocess.run(command, check=True, shell=True)

start_new_line = False
last_line = ""
simh_master_fd = None
simh_p = None
simh_t = None
wait_for_change = False
enable_print = False

def start_simh(input_file):
    global simh_master_fd
    global simh_p
    global simh_t
    master_fd, slave_fd = pty.openpty()
    tty.setraw(master_fd)
    tty.setraw(slave_fd)

    simh_master_fd = master_fd

    p = Popen(["pdp11", input_file], stdin=slave_fd, stdout=slave_fd, stderr=slave_fd)
    simh_p = p

    def read_stdout():
        global start_new_line
        global last_line
        global simh_master_fd
        global wait_for_change
        global enable_print
        sys.stdout.flush()
        while simh_p.poll() is None:
            r, w, e = select.select([simh_master_fd], [], [])
            if simh_master_fd in r:
                o = os.read(simh_master_fd, 1)
                if o:
                    data = o.decode("ascii")
                    wait_for_change = False
                    if (int.from_bytes(o, "big") >= 32 or o == b"\n") and enable_print:
                        print(data, end="")
                    if start_new_line:
                        start_new_line = False
                        last_line = data
                    else:
                        last_line += data
                    if data == "\n":
                        start_new_line = True
                    sys.stdout.flush()
            # sleep(0.001)
    
    t = Thread(target=read_stdout, args=())
    t.daemon = True # thread dies with the program
    t.start()

    simh_t = t

def run_when(prompt, command):
    global wait_for_change
    while last_line != prompt or wait_for_change:
        # print("\nLine:", last_line.encode("utf-8"), file=sys.stderr)
        sys.stdout.flush()
        sleep(0.1)
    os.write(simh_master_fd, command)
    # print(command.decode("utf-8"), end="")
    sys.stdout.flush()
    wait_simh_stop = True

def wait_simh_stop():
    simh_p.wait()
    # simh_t.join()
    start_new_line = True
    last_line = ""

def force_print(text):
    print(text)
    sys.stdout.flush()

if __name__ == "__main__":
    text_file = open("/aoc/aoc.mac", "r")
    x = text_file.read().encode("ascii")
    text_file.close()

    start_simh("boot.ini")
    # enable_print = True

    run_when("Overwrite last track? [N]", b"N\n")
    run_when(".", b"SET EDIT EDIT\n")
    sleep(0.1)
    run_when(".", b"EDIT/CREATE PROG.MAC\n")
    run_when("*", b"I")

#     x = b""".TITLE HELLO
# .MCALL .PRINT,.EXIT   ; tell assembler I want these two from SYSMAC.SML
#
# START:  .PRINT #HELLO ; call OS function to print string, address HELLO
#         .EXIT         ; call OS function to terminate the program
#
# HELLO: .ASCIZ /HELLO, WORLD/ ; an ASCII string ending with a zero byte
#
# .END START
# """
    run_when("*I", x)
    run_when(x.decode("ascii").splitlines()[-1] + "\n", b"\x1b\x1b")
    run_when("*", b"EX\x1b\x1b")
    # run_when(".", b"DIR HELLO.MAC\n")
    run_when(".", b"MACRO PROG/LIST/CROSSREFERENCE\n")
    sleep(0.1)
    run_when(".", b"LINK PROG/MAP\n")
    sleep(0.1)
    enable_print = True
    run_when(".", b"RUN PROG\n")


    sleep(0.5)
    run_when(".", b"\x05")
    enable_print = False
    run_when("sim> ", b"quit\n")

    wait_simh_stop()
    print()

