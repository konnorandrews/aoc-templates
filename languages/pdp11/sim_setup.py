import subprocess
from subprocess import Popen, PIPE
from time import sleep
import sys
from subprocess import PIPE, Popen
from threading  import Thread
from queue import Queue, Empty
import delegator
import os
import select
import pty
import tty

def run_command(command):
    subprocess.run(command, check=True, shell=True)

start_new_line = False
last_line = ""
simh_master_fd = None
simh_p = None
simh_t = None
wait_for_change = False

def start_simh(input_file):
    global simh_master_fd
    global simh_p
    global simh_t
    master_fd, slave_fd = pty.openpty()
    tty.setraw(master_fd)
    tty.setraw(slave_fd)

    simh_master_fd = master_fd

    p = Popen(["pdp11", input_file], stdin=slave_fd, stdout=slave_fd, stderr=slave_fd)
    simh_p = p

    def read_stdout():
        global start_new_line
        global last_line
        global simh_master_fd
        global wait_for_change
        sys.stdout.flush()
        while simh_p.poll() is None:
            r, w, e = select.select([simh_master_fd], [], [])
            if simh_master_fd in r:
                o = os.read(simh_master_fd, 1)
                if o:
                    data = o.decode("ascii")
                    wait_for_change = False
                    if int.from_bytes(o, "big") >= 32 or o == b"\n":
                        print(data, end="")
                    if start_new_line:
                        start_new_line = False
                        last_line = data
                    else:
                        last_line += data
                    if data == "\n":
                        start_new_line = True
                    sys.stdout.flush()
            # sleep(0.001)
    
    t = Thread(target=read_stdout, args=())
    t.daemon = True # thread dies with the program
    t.start()

    simh_t = t

def run_when(prompt, command):
    global wait_for_change
    while last_line != prompt or wait_for_change:
        # print("\nLine:", last_line.encode("utf-8"), file=sys.stderr)
        sys.stdout.flush()
        sleep(0.1)
    os.write(simh_master_fd, command)
    print(command.decode("utf-8"), end="")
    sys.stdout.flush()
    wait_simh_stop = True

def wait_simh_stop():
    simh_p.wait()
    # simh_t.join()
    start_new_line = True
    last_line = ""

def force_print(text):
    print(text)
    sys.stdout.flush()

if __name__ == "__main__":
    # Setup steps taken from:
    # http://decuser.blogspot.com/2015/12/tutorial-setting-up-rt-11-v53-on-simh.html

    # run_command("curl -O ftp://minnie.tuhs.org/pub/PDP-11/Sims/Supnik_2.3/software/rtv53swre.tar.gz")
    # run_command("curl -O http://www.dbit.com/pub/pdp11/empty/rl02.dsk.gz")

    run_command("tar xvf rtv53swre.tar.gz Disks/rtv53_rl.dsk")
    run_command("gunzip rl02.dsk.gz")

    run_command("mv rl02.dsk Disks/empty-rl02.dsk")
    run_command("rm rtv53swre.tar.gz")

    run_command("cp Disks/empty-rl02.dsk distribution-backup.dsk")
    run_command("cp Disks/rtv53_rl.dsk distribution.dsk")

    run_command("""cat >initial.ini <<"EOF"
set cpu 11/23+ 256K
set tto 8b
attach LPT lpt.txt
set rl0 writeenabled
set rl0 rl02
attach rl0 distribution.dsk
set rl1 writeenabled
set rl1 rl02
attach rl1 distribution-backup.dsk
set rl1 badblock
boot rl0
EOF""")

    run_command("""cat >boot.ini <<"EOF"
set cpu 11/23+ 256K
set tto 8b
attach LPT lpt.txt
set rl0 writeenabled
set rl0 rl02
attach rl0 working.dsk
set rl1 writeenabled
set rl1 rl02
attach rl1 storage.dsk
set rl1 badblock
boot rl0
EOF""")

    start_simh("initial.ini")

    run_when("Overwrite last track? [N]", b"Y\n")
    run_when("        Press the \"RETURN\" key when ready to continue. ", b"\n")
    run_when("        (Type YES or NO and press the \"RETURN\" key): ", b"YES\n")
    run_when("        Press the \"RETURN\" key when ready to continue. ", b"\n")
    run_when("              ", b"19-JAN-88\n")
    run_when("        Press the \"RETURN\" key when you have mounted the disk. ", b"\n")
    run_when("        Press the \"RETURN\" key when you have removed the disk. ", b"\x05")
    run_when("sim> ", b"detach rl1\n")
    run_when("sim> sim> ", b"! cp distribution-backup.dsk RT-11_V5.3_BIN_RL02_BACKUP\n")
    run_when("sim> sim> sim> ", b"! cp Disks/empty-rl02.dsk  working.dsk\n")
    run_when("sim> sim> sim> sim> ", b"attach rl1 working.dsk\n")
    run_when("sim> sim> sim> sim> sim> ", b"c\n")
    sleep(0.1)
    run_when("sim> sim> sim> sim> sim> ", b"\n")
    run_when("        Press the \"RETURN\" key when you have mounted the disk. ", b"\n")
    run_when("        Press the \"RETURN\" key when ready to continue. ", b"\n")
    run_when(".", b"\x05")
    run_when("sim> ", b"detach rl1\n")
    run_when("sim> sim> ", b"! cp working.dsk RT-11_V5.3_BIN_RL02_WORKING\n")
    run_when("sim> sim> sim> ", b"quit\n")

    # run_when("xxx", b"\n")

    wait_simh_stop()

    run_command("rm distribution.dsk distribution-backup.dsk")
    run_command("mv RT-11_V5.3_BIN_RL02_BACKUP Disks/")
    run_command("mv RT-11_V5.3_BIN_RL02_WORKING Disks/")
    run_command("rm initial.ini")
    run_command("cp Disks/empty-rl02.dsk storage.dsk")
    run_command("ls")

