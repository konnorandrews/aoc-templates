use anyhow::{bail, Context, Result};
use argh::FromArgs;
use serde::{Deserialize, Serialize};
use std::{
    collections::HashMap,
    env::current_dir,
    fs::write,
    iter::empty,
    path::{Path, PathBuf},
    process::Command,
};

#[derive(FromArgs, Debug)]
/// AoC Runner.
struct Args {
    #[argh(subcommand)]
    nested: Commands,
}

#[derive(FromArgs, PartialEq, Debug)]
#[argh(subcommand)]
enum Commands {
    Init(InitCommand),
    New(NewCommand),
    Run(RunCommand),
}

#[derive(FromArgs, PartialEq, Debug)]
/// Init AoC project.
#[argh(subcommand, name = "init")]
struct InitCommand {
    #[argh(positional)]
    path: Option<PathBuf>,
}

#[derive(FromArgs, PartialEq, Debug)]
/// Create new project for spesific day.
#[argh(subcommand, name = "new")]
struct NewCommand {}

#[derive(FromArgs, PartialEq, Debug)]
/// Run AoC solution.
#[argh(subcommand, name = "run")]
struct RunCommand {
    #[argh(positional)]
    input: Option<PathBuf>,
}

#[derive(Deserialize, Serialize, Debug)]
struct Config {
    days: HashMap<String, String>,
    languages: Languages,
}

#[derive(Deserialize, Serialize, Debug)]
struct Languages {
    possible: Vec<String>,
}

fn main() -> Result<()> {
    let args: Args = argh::from_env();

    match args.nested {
        Commands::Init(args) => do_init(&args)?,
        Commands::New(args) => todo!(),
        Commands::Run(args) => do_run(&args)?,
    }

    Ok(())
}

fn load_config() -> Result<Config> {
    Ok(toml::from_str(&std::fs::read_to_string(
        find_aoc_config().context("No AoC config")?,
    )?)?)
}

fn find_aoc_config() -> Option<PathBuf> {
    let mut current = std::env::current_dir().ok();

    while let Some(mut current_path) = current.clone() {
        current_path.push("aoc.toml");
        if current_path.is_file() {
            return Some(current_path);
        }
        current = current_path.parent()?.parent().map(Path::to_path_buf);
    }

    None
}

fn do_run(args: &RunCommand) -> Result<()> {
    let config = load_config()?;

    let current = std::env::current_dir()?;
    let day_name = current
        .file_name()
        .context("Not in day folder")?
        .to_string_lossy()
        .into_owned();
    let Some(language) = config.days.get(&day_name) else { bail!("Folder not in AoC config"); };
    dbg!(language);

    let output = Command::new("docker").args(["images"]).output()?;
    let output = String::from_utf8_lossy(&output.stdout).into_owned();
    if output
        .matches(&format!("aoc-{}", language))
        .next()
        .is_none()
    {
        println!("Docker container for {} is missing", language);

        let dockerfile = find_language_dockerfile(language)?;

        if !Command::new("docker")
            .args([
                "build",
                "-t",
                &format!("aoc-{}:latest", language),
                &dockerfile.parent().unwrap().to_string_lossy(),
            ])
            .status()?
            .success()
        {
            bail!("");
        }
    }

    if !Command::new("docker")
        .args([
            "run",
            "--rm",
            "-v",
            &format!("{}:/aoc", current_dir()?.to_string_lossy()),
        ])
        .args(if let Some(input) = &args.input {
            itertools::Either::Left(
                [
                    "-v".into(),
                    format!(
                        "{}:/input.txt:ro",
                        input.canonicalize()?.to_string_lossy()
                    ),
                ]
                .into_iter(),
            )
        } else {
            itertools::Either::Right(empty())
        })
        .arg(format!("aoc-{}:latest", language))
        .status()?
        .success()
    {
        bail!("");
    }

    Ok(())
}

fn find_language_dockerfile(name: &str) -> Result<PathBuf> {
    let mut current = std::env::current_exe()?;
    current.pop();
    current.pop();
    current.pop();
    current.push("languages");
    current.push(name);
    current.push("Dockerfile");
    assert!(current.is_file());
    Ok(current)
}

fn do_init(args: &InitCommand) -> Result<()> {
    let config_path = find_aoc_config();
    if let Some(config_path) = config_path {
        bail!(
            "AoC config file already exists at \"{}\". Cannot init a new AoC workspace.",
            config_path.to_string_lossy()
        );
    }

    let mut current = args.path.to_owned().unwrap_or(std::env::current_dir()?);
    current.push("aoc.toml");

    write(current, include_str!("../config_default.toml"))?;

    Ok(())
}
